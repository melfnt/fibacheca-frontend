import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Board, Image } from '@app/models';
import { Observable, of } from 'rxjs';
import { endpoints } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class BoardService {
  
  constructor( private service: HttpClient ) { }

  getBoard(id: string): Observable<Board> {
    // TODO server call
    return of({
      id,
      name: "Laurea Lucio",
    });
  }

  public postImage (description: string, title: string, name: string, buffer: string): Observable<Image> {
    console.debug ("[BOARD-SERVICE] first 50 chars of buffer", buffer.substr(0,50));
    return this.service.post <Image> (endpoints.postImage, {
        description,
        title,
        name,
        buffer
      },
      { headers: {'Content-Type': 'application/json'} }
    );
  }

}
