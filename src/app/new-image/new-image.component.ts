import { Component, OnInit } from '@angular/core';
import { BoardService } from '@app/board/board.service';
import { Observable } from 'rxjs';
import { Board } from '@app/models';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-new-image',
  templateUrl: './new-image.component.html',
  styleUrls: ['./new-image.component.scss']
})
export class NewImageComponent implements OnInit {

  board$: Observable<Board>;
  loading = false;
  title: string;
  description: string;
  name: string;
  fname: string;
  fbuffer: string | null = null;

  constructor( private service: BoardService, private _snackBar: MatSnackBar ) { }

  ngOnInit(): void {
    this.board$ = this.service.getBoard ("42");
  }

  reset() {
    this.description = "";
    this.title = "";
    this.name = "";
    this.fname = "";
    this.fbuffer = null;
  }

  upload_meme (file_elm: HTMLInputElement) {
    file_elm.click ();
  }

  file_chosen (file_elm: HTMLInputElement) {
    if (file_elm.files && file_elm.files[0]) {
      const f = file_elm.files[0];
      if (f.size > 5_000_000) {
        console.log ("[NEW-IMAGE] file too big");
        this._snackBar.open ("Il file è troppo grande e non ci passa per la porta del server", "Ne cerco uno più piccolo", {panelClass: "warn"});
      }
      else {
        this.fname = f.name;
        const reader = new FileReader();
        reader.onload = this._file_uploaded.bind(this);
        reader.readAsDataURL(f);
      }
    }
  }

  private _file_uploaded (fileLoadedEvent: ProgressEvent<FileReader>)
  {
    if ( fileLoadedEvent.target == null ) {
      this.fname = "";
      console.log ("[NEW-IMAGE] error while uploading the file");
      this._snackBar.open ("Errore nel caricamento del meme. Ma che combini? ", "Boh", {panelClass: "warn"});    
    }
    else {
      this.fbuffer = fileLoadedEvent.target.result as string;
    }
  }

  submit () {
    if (this.fbuffer) {
      this.loading = true;
      this.service.postImage (this.description, this.title, this.name, this.fbuffer)
      .subscribe ( (img) => {
        this._snackBar.open ("Meme caricato correttamente. Ora puoi tornare a scaccolarti o caricarne un altro", "", 
          {duration: 5000, panelClass:"success"});
        this.reset ();
        this.loading = false;
      },
      (e) => {
        this._snackBar.open ("Errore nel caricamento del meme. Ma che combini? " + e, "Farò il bravo, lo prometto", {panelClass: "warn"});
      })
    }
  }

}
