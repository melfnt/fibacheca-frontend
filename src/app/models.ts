
export interface Board {
    id: string;
    name: string;
}

export interface Image {
    id: string,
    url: string,
    title: string,
    user: string,
    date: string,
    description: string
}
