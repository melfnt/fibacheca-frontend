
import { NgModule } from '@angular/core';

import {MatFormFieldModule} from '@angular/material/form-field'; 
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
// import {MatTabsModule} from '@angular/material/tabs'; 
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatCardModule} from '@angular/material/card'; 
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner'; 

@NgModule({
    exports: [
      MatFormFieldModule,
      MatInputModule,
      MatButtonModule,
      // MatTabsModule,
      MatSnackBarModule,
      MatCardModule,
      MatIconModule,
      MatTooltipModule,
      MatProgressSpinnerModule,
    ]
})
export class MaterialImportModule {}
